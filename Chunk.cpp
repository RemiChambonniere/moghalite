#include "Chunk.hpp"

Chunk::Chunk(int start_x, int start_y, int CHUNK_SIZE, int halite, bool dropoff) : borders{ start_x, start_x + CHUNK_SIZE, start_y, start_y + CHUNK_SIZE } {
	this->center = Position(start_x + CHUNK_SIZE / 2, start_y + CHUNK_SIZE / 2);
	this->CHUNK_SIZE = CHUNK_SIZE;
	this->halite = halite;
	this->dropoff = dropoff;

	this->nb_ships = 0;
	this->last_update_turn = 0;
}

bool Chunk::isInChunk(shared_ptr<Entity> entity) {
	Position entity_pos = entity->position;
	return entity_pos.x > (center.x - 2) && entity_pos.x < (center.x + 2) && entity_pos.y > (center.y - 2) && entity_pos.y < (center.y + 2);
}

Halite Chunk::updateHalite(unique_ptr<GameMap>& game_map, int turn) {
	int total_halite = 0;
	for (int x = borders[0]; x < borders[1]; x++) {
		for (int y = borders[2]; y < borders[3]; y++) {
			total_halite += game_map->at(Position(x, y))->halite;
		}
	}
	this->last_update_turn = turn;
	return this->halite = total_halite / (CHUNK_SIZE * CHUNK_SIZE);
}