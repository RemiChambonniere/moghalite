#pragma once
#include <hlt/position.hpp>
#include <hlt/entity.hpp>
#include <hlt/game_map.hpp>

using namespace std;
using namespace hlt;

struct Chunk
{
	int CHUNK_SIZE;
	Position center;
	int nb_ships;
	int borders[4];
	int halite;
	bool dropoff;

	int last_update_turn;

	Chunk(int, int, int, int, bool);

	bool isInChunk(shared_ptr<Entity>);

	Halite updateHalite(unique_ptr<GameMap>&, int);
};