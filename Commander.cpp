#include "Commander.hpp"

#include <hlt/game.hpp>

void Commander::Init(vector<Position>* position_choices, vector<Command>* command_queue) {
	this->position_choices = position_choices;
	this->command_queue = command_queue;

	ships.clear();
	moving_ships.clear();
}

void Commander::CheckCanMove(unique_ptr<GameMap>& game_map) {
	for (const shared_ptr<Ship> ship : ships) {
		const Position ship_pos = ship->position;
		if (ship->halite < game_map->at(ship_pos)->halite / 10)
		{
			position_choices->push_back(ship_pos);
			command_queue->push_back(ship->stay_still());
		}
		else {
			moving_ships.push_back(ship);
		}
	}
}

void Commander::FindNearestSafeWay(unique_ptr<GameMap>& game_map, shared_ptr<Ship> ship, Position target_pos, PlayerId player_id, Direction* next_dir, Position* next_pos) {
	int min_halite = 10000;
	for (Direction dir : game_map->get_unsafe_moves(ship->position, target_pos)) {
		Position possible_pos = ship->position.directional_offset(dir);
		if (std::find(position_choices->begin(), position_choices->end(), possible_pos) != position_choices->end()) {
		}
		else {
			bool is_safe = true;
			for (Position pos : possible_pos.get_surrounding_cardinals()) {
				MapCell* cell = game_map->at(pos);
				if (cell->is_occupied() && cell->ship->owner != player_id) {
					is_safe = false;
					break;
				}
			}
			if (is_safe) {
				int temp_halite = game_map->at(possible_pos)->halite;
				if (min_halite > temp_halite) {
					*next_dir = dir;
					*next_pos = possible_pos;
					min_halite = temp_halite;
				}
			}
		}
	}
}