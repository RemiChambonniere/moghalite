#pragma once
#include <hlt/game.hpp>
#include <hlt/constants.hpp>
#include <hlt/log.hpp>

#include <map>

using namespace std;
using namespace hlt;

enum class Commander_Name {
	FARMER,
	TO_DROPOFF,
	EXPLORER,
	FINALRUSH,
};

struct Commander
{
	array<Direction, 5> direction_order = { Direction::NORTH, Direction::SOUTH, Direction::EAST, Direction::WEST, Direction::STILL };

	vector<shared_ptr<Ship>> ships;
	vector<shared_ptr<Ship>> moving_ships;

	vector<Position>* position_choices;
	vector<Command>* command_queue;

	virtual void Init(vector<Position>*, vector<Command>*);

	virtual void CheckCanMove(unique_ptr<GameMap>&);

	virtual void FindNearestSafeWay(unique_ptr<GameMap>&, shared_ptr<Ship>, Position, PlayerId, Direction*, Position*);

	virtual void doAction(Game*, unique_ptr<GameMap>&) = 0;

	virtual Commander_Name getId() = 0;

	virtual Commander_Name nextState(shared_ptr<Ship>) = 0;

	virtual void removeShip(EntityId) {};
};