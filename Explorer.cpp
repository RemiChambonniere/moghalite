#include "Explorer.hpp"

#include <map>

using namespace std;
using namespace hlt;

Explorer::Explorer(vector<Chunk>* chunks) {
	this->chunks = chunks;
	this->ships_chunks = map < EntityId, Chunk* >();
}

void Explorer::doAction(Game* game, unique_ptr<GameMap>& game_map) {
	for (const shared_ptr<Ship> ship : moving_ships) {
		map<EntityId, Chunk*>::iterator ship_chunk_it = ships_chunks.find(ship->id);

		if (ship_chunk_it != ships_chunks.end()) {
		}
		else {
			// find chunk
			float max_score = 0;
			Chunk* target = &(*chunks)[0];
			for (Chunk &chunk : *chunks) {
				if (chunk.nb_ships < 4) {
					float score = chunk.halite / (sqrt(game_map->calculate_distance(ship->position, chunk.center)) * 1.5);
					//log::log(to_string(chunk.halite) + ' ' + to_string(sqrt(game_map->calculate_distance(ship->position, chunk.center)) * 1.5) + ' ' + to_string(score));
					if (score > max_score) {
						if (chunk.last_update_turn < (game->turn_number - 4)) {
							chunk.updateHalite(game_map, game->turn_number);
							score = chunk.halite / (sqrt(game_map->calculate_distance(ship->position, chunk.center)) * 1.5);
							if (score > max_score) {
								max_score = score;
								target = &chunk;
							}
						}
						else {
							max_score = score;
							target = &chunk;
						}
					}
				}
			}
			target->nb_ships++;
			//log::log(to_string(target->center.x) + ' ' + to_string(target->center.y));
			ship_chunk_it = ships_chunks.insert(pair<EntityId, Chunk*>(ship->id, target)).first;
		}

		// navigate to chunk
		Direction* next_dir = new Direction(Direction::STILL);
		Position* next_pos = new Position(ship->position);

		FindNearestSafeWay(game_map, ship, ship_chunk_it->second->center, game->me->id, next_dir, next_pos);

		command_queue->push_back(ship->move(*next_dir));
		position_choices->push_back(*next_pos);
	}
}

Commander_Name Explorer::nextState(shared_ptr<Ship> ship) {
	Chunk* target = ships_chunks[ship->id];
	if (ship->halite > (constants::MAX_HALITE * 0.9)) {
		target->nb_ships--;
		return Commander_Name::TO_DROPOFF;
	}
	else if (target->isInChunk(ship)) {
		target->nb_ships--;
		return Commander_Name::FARMER;
	}
	else {
		return Commander_Name::EXPLORER;
	}
}

void Explorer::removeShip(EntityId ship_id) {
	ships_chunks[ship_id]->nb_ships--;
	ships_chunks.erase(ship_id);
}
