#pragma once

#include "Commander.hpp"

#include "Chunk.hpp"

using namespace hlt;

struct Explorer : Commander
{
    vector<Chunk>* chunks;
    map<EntityId, Chunk*> ships_chunks;

    Explorer(vector<Chunk>*);
    void doAction(Game*, unique_ptr<GameMap>&) override;
    Commander_Name getId() override { return Commander_Name::EXPLORER; };
    Commander_Name nextState(shared_ptr<Ship>) override;
    void removeShip(EntityId) override;
};

