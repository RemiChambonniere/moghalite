#include "Farmer.hpp"

#include <map>

using namespace std;
using namespace hlt;

void Farmer::doAction(Game* game, unique_ptr<GameMap>& game_map) {
    //if (game->turn_number % 2 == 0) {
    for (const shared_ptr<Ship> ship : moving_ships) {
        std::array<Position, 4> position_option_temp = ship->position.get_surrounding_cardinals();
        std::array<Position, 5> position_options = { position_option_temp[0], position_option_temp[1], position_option_temp[2], position_option_temp[3], ship->position };

        std::map<Direction, Position> position_dict;
        std::map<Direction, int> halite_dict;

        // Init near neighbours
        int n = 0;
        for (const Direction dir : direction_order) {
            Position position = position_options[n];

            position_dict[dir] = position;

            int halite_amount = game_map->at(position)->halite;
            halite_dict[dir] = halite_amount;

            n++;
        }

        // Init find the most halited neighbour
        Direction next_dir = Direction::STILL;
        Position next_pos = ship->position;
        float max_halite = 0;
        for (const auto& halite_iterator : halite_dict) {
            if ((halite_iterator.second * 0.8f) > max_halite) {
                bool is_safe = true;
                for (Position pos : position_dict[halite_iterator.first].get_surrounding_cardinals()) {
                    MapCell* cell = game_map->at(pos);
                    if (cell->is_occupied() && cell->ship->owner != game->me->id) {
                        is_safe = false;
                        break;
                    }
                }
                if (is_safe) {
                    // if position is not empty
                    if (std::find(position_choices->begin(), position_choices->end(), position_dict[halite_iterator.first]) != position_choices->end()) {
                    }
                    else {
                        next_dir = halite_iterator.first;
                        next_pos = position_dict[next_dir];
                        max_halite = halite_iterator.second * 0.8f;
                        if (next_dir == Direction::STILL) {
                            log::log(to_string(ship->id) + " : " + to_string(ship->halite));
                        }
                    }
                }
            }
        }
        position_choices->push_back(next_pos);

        // Plus une case a d'halite, plus ça coute de la quitter (10% de la valeur de la case de départ) -> FAIRE ROUTE

        command_queue->push_back(ship->move(next_dir));
    }
}

void Farmer::CheckCanMove(unique_ptr<GameMap>& game_map) {
    for (const shared_ptr<Ship> ship : ships) {
        const Position ship_pos = ship->position;
        Halite cell_halite = game_map->at(ship->position)->halite;
        if (cell_halite > ship->halite * 0.3 || ship->halite < cell_halite / 10)
        {
            position_choices->push_back(ship_pos);
            command_queue->push_back(ship->stay_still());
        }
        else {
            moving_ships.push_back(ship);
        }
    }
}

Commander_Name Farmer::nextState(shared_ptr<Ship> ship) {
    if (ship->halite > (constants::MAX_HALITE * 0.9)) {
        return Commander_Name::TO_DROPOFF;
    }
    else {
        return Commander_Name::FARMER;
    }
}