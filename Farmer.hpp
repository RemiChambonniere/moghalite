#pragma once

#include "Commander.hpp"

using namespace std;
using namespace hlt;

struct Farmer : Commander
{
    void doAction(Game*, unique_ptr<GameMap>&) override;
    Commander_Name getId() override { return Commander_Name::FARMER; };
    Commander_Name nextState(shared_ptr<Ship>) override;
    void CheckCanMove(unique_ptr<GameMap>&) override;
};

