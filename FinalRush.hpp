#pragma once

#include "Commander.hpp"

using namespace std;
using namespace hlt;

struct FinalRush : Commander
{
    void doAction(Game*, unique_ptr<GameMap>&) override;
    Commander_Name getId() override { return Commander_Name::FINALRUSH; };
    Commander_Name nextState(shared_ptr<Ship>) override;
};

