#include <random>
#include <ctime>

#include <map>

#include <sstream>

#include <Commander.hpp>
#include <Farmer.hpp>
#include <ToDropOff.hpp>
#include <Explorer.hpp>
#include <FinalRush.hpp>

#include <Chunk.hpp>

using namespace std;
using namespace hlt;

std::string toString(std::ostream& str)
{
    ostringstream ss;
    ss << str.rdbuf();
    return ss.str();
}

int NB_MAX_SHIPS = 12;
int CHUNK_SIZE = 8;

void clearDestroyedShips(map<EntityId, Commander*>* CommanderList, std::unordered_map<EntityId, std::shared_ptr<Ship>>* all_ships) {
    for (auto it = CommanderList->cbegin(), next_it = it; it != CommanderList->cend(); it = next_it)
    {
        ++next_it;
        if (all_ships->count(it->first) < 1)
        {
            it->second->removeShip(it->first);
            CommanderList->erase(it);
        }
    }
}

int main(int argc, char* argv[]) {
    unsigned int rng_seed;
    if (argc > 1) {
        rng_seed = static_cast<unsigned int>(stoul(argv[1]));
    }
    else {
        rng_seed = static_cast<unsigned int>(time(nullptr));
    }
    mt19937 rng(rng_seed);

    Game game;

    vector<Chunk> chunks;
    unique_ptr<GameMap>& game_map = game.game_map;

    for (int chunk_x = 0; chunk_x < game_map->width; chunk_x += CHUNK_SIZE) {
        for (int chunk_y = 0; chunk_y < game_map->height; chunk_y += CHUNK_SIZE) {
            Halite total_halite = 0;
            bool is_shipyard = false;
            for (int x = chunk_x; x < chunk_x + CHUNK_SIZE; x++) {
                for (int y = chunk_y; y < chunk_y + CHUNK_SIZE; y++) {
                    MapCell* cell = game_map->at(Position(x, y));
                    total_halite += cell->halite;
                    if (cell->position == game.me->shipyard->position) is_shipyard = true;
                }
            }
            chunks.push_back(Chunk(chunk_x, chunk_y, CHUNK_SIZE, total_halite / (CHUNK_SIZE * CHUNK_SIZE), is_shipyard));
        }
    }

    for (int chunk_x = -CHUNK_SIZE / 2; chunk_x < game_map->width + CHUNK_SIZE / 2; chunk_x += CHUNK_SIZE) {
        for (int chunk_y = -CHUNK_SIZE / 2; chunk_y < game_map->height + CHUNK_SIZE / 2; chunk_y += CHUNK_SIZE) {
            Halite total_halite = 0;
            bool is_shipyard = false;
            for (int x = chunk_x; x < chunk_x + CHUNK_SIZE; x++) {
                for (int y = chunk_y; y < chunk_y + CHUNK_SIZE; y++) {
                    MapCell* cell = game_map->at(Position(x, y));
                    total_halite += cell->halite;
                    if (cell->position == game.me->shipyard->position) is_shipyard = true;
                }
            }
            chunks.push_back(Chunk(chunk_x, chunk_y, CHUNK_SIZE, total_halite / (CHUNK_SIZE * CHUNK_SIZE), is_shipyard));
        }
    }

    map<EntityId, Commander*> CommanderList = map<EntityId, Commander*>();
    
    FinalRush* final_rush = new FinalRush();
    Farmer* farmer = new Farmer();
    ToDropOff* toDropOff = new ToDropOff();
    Explorer* explorer = new Explorer(&chunks);

    vector<Commander*> commanders;

    commanders.push_back(final_rush);
    commanders.push_back(farmer);
    commanders.push_back(toDropOff);
    commanders.push_back(explorer);

    map<Commander_Name, Commander*> commanders_map = {
        { Commander_Name::EXPLORER, explorer },
        { Commander_Name::FARMER, farmer },
        { Commander_Name::TO_DROPOFF, toDropOff }
    };

    // At this point "game" variable is populated with initial map data.
    // This is a good place to do computationally expensive start-up pre-processing.
    // As soon as you call "ready" function below, the 2 second per turn timer will start.
    game.ready("MogHalite");

    log::log("Successfully created bot! My Player ID is " + to_string(game.my_id) + ". Bot rng seed is " + to_string(rng_seed) + ".");

    for (;;) {
        game.update_frame();

        shared_ptr<Player> me = game.me;
        unique_ptr<GameMap>& game_map = game.game_map;

        vector<Command> command_queue;
        vector<Position> position_choices;

        for (Commander* commander : commanders) {
            commander->Init(&position_choices, &command_queue);
        }

        std::unordered_map<EntityId, std::shared_ptr<Ship>> all_ships = me->ships;

        if (game.turn_number % 5 == 0) {
            clearDestroyedShips(&CommanderList, &all_ships);
        }

        for (const auto& ship_iterator : all_ships) {
            shared_ptr<Ship> ship = ship_iterator.second;

            std::map<EntityId, Commander*>::iterator commander_iterator = CommanderList.find(ship->id);
            if (commander_iterator != CommanderList.end()) {
                if (game.turn_number + game_map->width * 0.75 > constants::MAX_TURNS) {
                    commander_iterator->second->removeShip(ship->id);
                    commander_iterator->second = final_rush;
                    final_rush->ships.push_back(ship);
                }
                else {
                    Commander_Name commander_id = commander_iterator->second->nextState(ship);
                    if (commander_id != commander_iterator->second->getId()) {
                        commander_iterator->second->removeShip(ship->id);
                        Commander* new_commander = commanders_map[commander_id];
                        commander_iterator->second = new_commander;
                        new_commander->ships.push_back(ship);
                    }
                    else {
                        commander_iterator->second->ships.push_back(ship);
                    }
                }
            } else {
                // Default is explorer
                commander_iterator = CommanderList.insert(pair<EntityId, Commander*>(ship->id, explorer)).first;
                explorer->ships.push_back(ship);
            }
        }

        for (Commander* commander : commanders) {
            commander->CheckCanMove(game_map);
        }

        for (Commander* commander : commanders) {
            commander->doAction(&game, game_map);
        }

        if (game.turn_number + game_map->width < constants::MAX_TURNS &&
            all_ships.size() < NB_MAX_SHIPS &&
            me->halite >= constants::SHIP_COST &&
            !(std::find(position_choices.begin(), position_choices.end(), game_map->at(me->shipyard)->position) != position_choices.end()))
        {
            command_queue.push_back(me->shipyard->spawn());
        }

        if (!game.end_turn(command_queue)) {
            break;
        }
    }

    return 0;
}
