#include "ToDropOff.hpp"

#include <map>

using namespace std;
using namespace hlt;

void ToDropOff::doAction(Game* game, unique_ptr<GameMap> &game_map) {
    Position shipyard_pos = game->me->shipyard->position;

    auto dropoffs = game->me->dropoffs;

    for (const shared_ptr<Ship> ship : moving_ships) {

        Position ship_pos = ship->position;
        Position target_pos = shipyard_pos;

        int min_dist = game_map->calculate_distance(ship_pos, shipyard_pos);

        for (const auto& dropoff_iterator : dropoffs) {
            int dropoff_dist = game_map->calculate_distance(ship_pos, dropoff_iterator.second->position);
            if (min_dist > dropoff_dist) {
                target_pos = dropoff_iterator.second->position;
                min_dist = dropoff_dist;
            }
        }

        Direction* next_dir = new Direction(Direction::STILL);
        Position* next_pos = new Position(ship_pos);

        FindNearestSafeWay(game_map, ship, target_pos, game->me->id, next_dir, next_pos);

        command_queue->push_back(ship->move(*next_dir));
        position_choices->push_back(*next_pos);
    }
}

Commander_Name ToDropOff::nextState(shared_ptr<Ship> ship) {
    if (ship->halite < 1) {
        return Commander_Name::EXPLORER;
    }
    else {
        return Commander_Name::TO_DROPOFF;
    }
}