#pragma once

#include "Commander.hpp"

using namespace std;
using namespace hlt;

struct ToDropOff : Commander
{
    void doAction(Game*, unique_ptr<GameMap>&) override;
    Commander_Name getId() override { return Commander_Name::TO_DROPOFF; };
    Commander_Name nextState(shared_ptr<Ship>) override;
};

